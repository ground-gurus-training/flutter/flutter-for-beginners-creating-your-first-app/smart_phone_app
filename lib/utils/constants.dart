import 'package:flutter/material.dart';

import '../pages/cameras_page.dart';
import '../pages/home/home_page.dart';
import '../pages/me_page.dart';
import '../pages/smart_page.dart';
import '../pages/vacuums_page.dart';

const navbarItems = <Widget>[
  NavigationDestination(
    icon: Icon(
      Icons.house_outlined,
    ),
    selectedIcon: Icon(
      Icons.house,
      color: Colors.blue,
    ),
    label: 'Home',
  ),
  NavigationDestination(
    icon: Icon(
      Icons.linked_camera_outlined,
    ),
    selectedIcon: Icon(
      Icons.linked_camera,
      color: Colors.blue,
    ),
    label: 'Cameras',
  ),
  NavigationDestination(
    icon: Icon(
      Icons.smart_toy_outlined,
    ),
    selectedIcon: Icon(
      Icons.smart_toy,
      color: Colors.blue,
    ),
    label: 'Vacuums',
  ),
  NavigationDestination(
    icon: Icon(
      Icons.camera_outlined,
    ),
    selectedIcon: Icon(
      Icons.camera,
      color: Colors.blue,
    ),
    label: 'Smart',
  ),
  NavigationDestination(
    icon: Icon(
      Icons.face_outlined,
    ),
    selectedIcon: Icon(
      Icons.face,
      color: Colors.blue,
    ),
    label: 'Me',
  ),
];

const pages = <Widget>[
  HomePage(),
  CamerasPage(),
  VacuumsPage(),
  SmartPage(),
  MePage()
];
