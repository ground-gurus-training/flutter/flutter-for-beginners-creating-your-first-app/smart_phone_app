import 'package:flutter/material.dart';

class MePage extends StatelessWidget {
  const MePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
        "MePage",
        style: TextStyle(
          fontSize: 30,
        ),
      ),
    );
  }
}
