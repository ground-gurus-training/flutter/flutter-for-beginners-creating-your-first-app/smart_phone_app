import 'package:flutter/material.dart';

class HomeWelcome extends StatelessWidget {
  const HomeWelcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: Row(
        children: const [
          Text(
            'My home',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(width: 8.0),
          Icon(
            Icons.swap_horiz,
            size: 32.0,
          ),
        ],
      ),
    );
  }
}
