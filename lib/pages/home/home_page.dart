import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:smart_home_app/pages/home/home_navbar.dart';
import 'package:smart_home_app/pages/home/home_welcome.dart';

import 'favorites/favorites.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height / 4,
          width: MediaQuery.of(context).size.width,
          child: Opacity(
            opacity: 0.3,
            child: Image.asset(
              'images/living_room.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 4,
            sigmaY: 4,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.0),
            ),
          ),
        ),
        Column(
          children: const [
            HomeNavbar(),
            HomeWelcome(),
            Favorites(),
          ],
        ),
      ],
    );
  }
}
