import 'package:flutter/material.dart';

class HomeNavbar extends StatelessWidget {
  const HomeNavbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: const [
          Icon(
            Icons.email_outlined,
          ),
          SizedBox(
            width: 16,
          ),
          Icon(
            Icons.add_circle_outline,
          ),
        ],
      ),
    );
  }
}
