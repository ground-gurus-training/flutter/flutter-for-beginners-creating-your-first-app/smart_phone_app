import 'package:flutter/material.dart';

class FavoritesHeader extends StatelessWidget {
  const FavoritesHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(
          height: 20,
          child: VerticalDivider(
            thickness: 4,
            color: Colors.indigo,
          ),
        ),
        const Text(
          "FAVORITES",
          style: TextStyle(fontSize: 18.0),
        ),
        Expanded(
          child: Container(),
        ),
        OutlinedButton(
          onPressed: () => {},
          child: const Text(
            'ALL',
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
