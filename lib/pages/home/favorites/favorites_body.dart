import 'package:flutter/material.dart';
import 'package:smart_home_app/pages/home/favorites/favorites_card.dart';

class FavoritesBody extends StatelessWidget {
  const FavoritesBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: const [
          Expanded(
            child: SizedBox(
              height: 150,
              child: FavoritesCard(),
            ),
          ),
          SizedBox(width: 8.0),
          Expanded(
            child: SizedBox(
              height: 150,
              child: FavoritesCard(),
            ),
          ),
        ],
      ),
    );
  }
}
