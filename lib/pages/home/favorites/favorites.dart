import 'package:flutter/material.dart';
import 'package:smart_home_app/pages/home/favorites/favorites_body.dart';
import 'package:smart_home_app/pages/home/favorites/favorites_header.dart';

class Favorites extends StatelessWidget {
  const Favorites({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: const [
          FavoritesHeader(),
          FavoritesBody(),
        ],
      ),
    );
  }
}
