import 'package:flutter/material.dart';

var myTheme = ThemeData(
  brightness: Brightness.light,
  colorScheme: ColorScheme.fromSwatch().copyWith(
    primary: Colors.lightBlue[200],
    secondary: Colors.cyan[600],
    tertiary: Colors.grey[300],
  ),
  textTheme: const TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    bodyText2: TextStyle(fontSize: 32.0),
  ),
  scaffoldBackgroundColor: Colors.grey[100],
  iconTheme: const IconThemeData(size: 32.0),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      foregroundColor: Colors.lightBlue[200],
      textStyle: TextStyle(color: Colors.grey[200]),
      backgroundColor: Colors.grey[100],
    ),
  ),
);
